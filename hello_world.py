import sys

from django.conf import settings
from django.urls import include,path
from django.http import HttpResponse
from django.core.management import execute_from_command_line

settings.configure(
    DEBUG=True,
    SECRET_KEY='thisisntagreatkeybutitwilldo',
    ROOT_URLCONF=sys.modules[__name__],
)

class views():
    def index(request):
        return HttpResponse("Hello, World")

urlpatterns = [
    path('hello-world/', views.index, name='hello-world-view')
]

if __name__ == "__main__":
    execute_from_command_line(sys.argv)